'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _service = require('@crow/service');

var _zh_CN = require('antd/lib/locale-provider/zh_CN');

var _zh_CN2 = _interopRequireDefault(_zh_CN);

var _en_US = require('antd/lib/locale-provider/en_US');

var _en_US2 = _interopRequireDefault(_en_US);

var _it_IT = require('antd/lib/locale-provider/it_IT');

var _it_IT2 = _interopRequireDefault(_it_IT);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

// 无目标语言时的默认语言
var defaultLang = 'zh_hans';

module.exports = function factory(baseUrl) {
  var i18nService = (0, _service.parrotI18nService)(baseUrl);
  return {
    namespace: 'i18n',

    state: {
      // 当前语言
      currentLang: 'zh_hans',
      // 无目标语言时的 Antd 默认语言包
      defaultLangAntdProvider: _zh_CN2.default,
      // 可用语言清单
      langOptions: [
        // {
        //   name: 'zh_hans',
        //   lang: '中文'
        // }
      ],
      // Antd 当前语言包
      currentLangAntdProvider: _zh_CN2.default,
      // Antd 的可用语言包映射
      antdProviderMap: {
        'en': _en_US2.default,
        'zh_hans': _zh_CN2.default,
        'it': _it_IT2.default
      },
      // 当前语言的语言包
      map: {
        // key: 'value'
      }
    },

    effects: {

      // API 错误处理，默认转发到全局
      // * 可供外部覆写
      APIErrorMessage: /*#__PURE__*/regeneratorRuntime.mark(function APIErrorMessage(_ref, _ref2) {
        var payload = _ref.payload;
        var put = _ref2.put;
        return regeneratorRuntime.wrap(function APIErrorMessage$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return put({ type: 'global/APIErrorMessage', payload: payload });

              case 2:
              case 'end':
                return _context.stop();
            }
          }
        }, APIErrorMessage, this);
      }),


      // 语言更新成功时执行，默认什么都不做
      // * 可供外部覆写
      switchLangSuccessful: function switchLangSuccessful(state, _ref3) {
        var payload = _ref3.payload;

        // payload.needsSwitch
        // payload.silent
        return state;
      },


      // 获取可用的语言包
      fetchLangOptions: /*#__PURE__*/regeneratorRuntime.mark(function fetchLangOptions(_ref4, _ref5) {
        var _ref4$payload = _ref4.payload,
            payload = _ref4$payload === undefined ? {} : _ref4$payload;
        var call = _ref5.call,
            put = _ref5.put,
            select = _ref5.select;
        var res;
        return regeneratorRuntime.wrap(function fetchLangOptions$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return call(i18nService.fetchSupportedLanguages);

              case 2:
                res = _context2.sent;

                if (!res.success) {
                  _context2.next = 8;
                  break;
                }

                _context2.next = 6;
                return put({ type: 'setLangOptions', payload: res.data });

              case 6:
                _context2.next = 10;
                break;

              case 8:
                _context2.next = 10;
                return put({ type: 'APIErrorMessage', payload: res });

              case 10:
              case 'end':
                return _context2.stop();
            }
          }
        }, fetchLangOptions, this);
      }),


      // 切换语言 / 更新当前语言包
      switchLang: /*#__PURE__*/regeneratorRuntime.mark(function switchLang(_ref6, _ref7) {
        var _ref6$payload = _ref6.payload,
            payload = _ref6$payload === undefined ? {} : _ref6$payload;
        var call = _ref7.call,
            put = _ref7.put,
            select = _ref7.select;

        var _ref8, currentLang, langOptions, antdProviderMap, defaultLangAntdProvider, _payload$lang, lang, _payload$silent, silent, needsSwitch, antdLocaleProvider, res;

        return regeneratorRuntime.wrap(function switchLang$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return select(function (_ref9) {
                  var i18n = _ref9.i18n;
                  return i18n;
                });

              case 2:
                _ref8 = _context3.sent;
                currentLang = _ref8.currentLang;
                langOptions = _ref8.langOptions;
                antdProviderMap = _ref8.antdProviderMap;
                defaultLangAntdProvider = _ref8.defaultLangAntdProvider;

                // 当传入lang时切换语言，否则为更新语言文案
                _payload$lang = payload.lang, lang = _payload$lang === undefined ? null : _payload$lang, _payload$silent = payload.silent, silent = _payload$silent === undefined ? false : _payload$silent;
                needsSwitch = !!(lang && lang !== currentLang);
                // 确定目标语言

                currentLang = needsSwitch ? lang : currentLang;

                antdLocaleProvider = antdProviderMap[lang];

                // 若目标语言不在支持列表内，则使用默认语言

                if (!langOptions.map(function (_) {
                  return _.lang;
                }).includes(currentLang)) currentLang = defaultLang;
                if (!antdLocaleProvider) antdLocaleProvider = defaultLangAntdProvider;
                // 获取语言包
                _context3.next = 15;
                return call(i18nService.fetchWordsMap, currentLang);

              case 15:
                res = _context3.sent;

                if (!res.success) {
                  _context3.next = 23;
                  break;
                }

                _context3.next = 19;
                return put({ type: 'setCurrentLang', payload: {
                    currentLang: currentLang,
                    currentLangAntdProvider: antdLocaleProvider,
                    map: res.data
                  } });

              case 19:
                _context3.next = 21;
                return put({ type: 'switchLangSuccessful', payload: {
                    needsSwitch: needsSwitch,
                    silent: silent
                  } });

              case 21:
                _context3.next = 25;
                break;

              case 23:
                _context3.next = 25;
                return put({ type: 'APIErrorMessage', payload: res });

              case 25:
              case 'end':
                return _context3.stop();
            }
          }
        }, switchLang, this);
      })
    },

    reducers: {
      setLangOptions: function setLangOptions(state, _ref10) {
        var payload = _ref10.payload;

        return _extends({}, state, { langOptions: payload });
      },
      setCurrentLang: function setCurrentLang(state, _ref11) {
        var payload = _ref11.payload;

        return _extends({}, state, {
          currentLang: payload.currentLang,
          currentLangAntdProvider: payload.currentLangAntdProvider,
          map: payload.map
        });
      }
    },

    subscriptions: {
      setup: function setup(_ref12) {
        var _this = this;

        var dispatch = _ref12.dispatch,
            history = _ref12.history;
        return _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
          return regeneratorRuntime.wrap(function _callee$(_context4) {
            while (1) {
              switch (_context4.prev = _context4.next) {
                case 0:
                  _context4.next = 2;
                  return dispatch({ type: 'fetchLangOptions' });

                case 2:
                  _context4.next = 4;
                  return dispatch({ type: 'switchLang', payload: { silent: true } });

                case 4:
                case 'end':
                  return _context4.stop();
              }
            }
          }, _callee, _this);
        }))();
      }
    }

  };
};