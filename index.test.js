import intro from './index';

test('module export should be an object', () => {
  expect(typeof intro).toBe('object');
});

test('should export modelExtend', () => {
  expect(typeof intro.modelExtend).toBe('function');
});

