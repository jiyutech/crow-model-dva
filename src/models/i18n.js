import { parrotI18nService } from '@crow/service';

import zhCN from 'antd/lib/locale-provider/zh_CN';
import enUS from 'antd/lib/locale-provider/en_US';
import itIT from 'antd/lib/locale-provider/it_IT';

// 无目标语言时的默认语言
const defaultLang = 'zh_hans';

module.exports = function factory( baseUrl ){
  let i18nService = parrotI18nService(baseUrl);
  return {
    namespace: 'i18n',

    state: {
      // 当前语言
      currentLang: 'zh_hans',
      // 无目标语言时的 Antd 默认语言包
      defaultLangAntdProvider: zhCN,
      // 可用语言清单
      langOptions: [
        // {
        //   name: 'zh_hans',
        //   lang: '中文'
        // }
      ],
      // Antd 当前语言包
      currentLangAntdProvider: zhCN,
      // Antd 的可用语言包映射
      antdProviderMap: {
        'en': enUS,
        'zh_hans': zhCN,
        'it': itIT,
      },
      // 当前语言的语言包
      map: {
        // key: 'value'
      },
    },

    effects: {

      // API 错误处理，默认转发到全局
      // * 可供外部覆写
      *APIErrorMessage({ payload }, { put }){
        yield put({ type: 'global/APIErrorMessage', payload: payload });
      },

      // 语言更新成功时执行，默认什么都不做
      // * 可供外部覆写
      switchLangSuccessful(state, {payload}) {
        // payload.needsSwitch
        // payload.silent
        return state;
      },

      // 获取可用的语言包
      *fetchLangOptions({ payload = {} }, { call, put, select }) {
        // 获取语言包
        const res = yield call(i18nService.fetchSupportedLanguages);
        if ( res.success ) {
          // 更新数据
          yield put({ type: 'setLangOptions', payload: res.data });
        }
        else yield put({ type: 'APIErrorMessage', payload: res });
      },

      // 切换语言 / 更新当前语言包
      *switchLang({ payload = {} }, { call, put, select }) {
        let { currentLang, langOptions, antdProviderMap, defaultLangAntdProvider } = yield select(({ i18n }) => (i18n));
        // 当传入lang时切换语言，否则为更新语言文案
        let {lang = null, silent = false } = payload;
        let needsSwitch = !!(lang && lang !== currentLang);
        // 确定目标语言
        currentLang = needsSwitch ? lang : currentLang;
        let antdLocaleProvider = antdProviderMap[lang];
        // 若目标语言不在支持列表内，则使用默认语言
        if ( !langOptions.map(_=>_.lang).includes(currentLang) ) currentLang = defaultLang;
        if ( !antdLocaleProvider ) antdLocaleProvider = defaultLangAntdProvider;
        // 获取语言包
        const res = yield call(i18nService.fetchWordsMap, currentLang);
        if ( res.success ) {
          // 更新数据
          yield put({ type: 'setCurrentLang', payload: {
            currentLang,
            currentLangAntdProvider: antdLocaleProvider,
            map: res.data,
          } });
          yield put({ type: 'switchLangSuccessful', payload: {
            needsSwitch,
            silent,
          } });
        }
        else yield put({ type: 'APIErrorMessage', payload: res });
      },
    },

    reducers: {
      setLangOptions(state, {payload}) {
        return { ...state, langOptions: payload };
      },
      setCurrentLang(state, {payload}) {
        return {
          ...state,
          currentLang: payload.currentLang,
          currentLangAntdProvider: payload.currentLangAntdProvider,
          map: payload.map
        };
      },
    },

    subscriptions: {
      async setup({ dispatch, history }) {
        await dispatch({type:'fetchLangOptions'})
        await dispatch({ type: 'switchLang', payload: { silent: true } })
      },
    }

  };
}
